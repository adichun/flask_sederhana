# import
from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS

# initial object flask
app = Flask(__name__)

# initial object flask restful
api = Api(app)

# initial object flask cors
CORS(app)

# initial variable empty a type dictionary
identitas = {}


class ContohResource(Resource):
    def get(self):
        return identitas

    def post(self):
        nama = request.form["nama"]
        umur = request.form["umur"]
        identitas["nama"] = nama
        identitas["umur"] = umur
        response = {"msg": "Data has been success"}
        return response


# setup resource
api.add_resource(ContohResource, "/api", methods=["GET", "POST"])

if __name__ == "__main__":
    app.run(debug=True, port=5005)
